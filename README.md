## Locations
- `./dogespinner/` -> https://aterlux.com/doge/ ...
- `./t-pages/` -> https://aterlux.com/t/ ...
- `./dev-index.html` -> https://aterlux.com/dev/index.html
- `./chris.html` -> https://aterlux.com/chris/ ...


## Todo
- Create an entire `nofont` / `nottf` variant of all the pages that use `deez_ponk.css` that isntead use `deez_ponk-no-ttf.css`
- - This should live somewhere like `https://aterlux.com/t-nottf` or `https://aterlux.com/t-fast`
