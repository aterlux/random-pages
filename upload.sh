#!/bin/bash


scp -r t-pages/ server-aterlux-superbia:/var/www/aterlux.com/t/
scp -r dogespinner/ server-aterlux-superbia:/var/www/aterlux.com/
scp chris.html server-aterlux-superbia:/var/www/aterlux.com/chris.html

scp dev-fe-index.html server-aterlux-superbia:/var/www/aterlux.com/dev-fe/index.html

scp index.html server-aterlux-superbia:/var/www/aterlux.com/index.html
scp main-yellowpages.html server-aterlux-superbia:/var/www/aterlux.com/yellowpages.html
scp favicon.ico server-aterlux-superbia:/var/www/aterlux.com/favicon.ico